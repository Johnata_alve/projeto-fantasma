#Top 10 países com maior soma de público de todas as partidas de todas as copas.

view(part)
publi <- part %>% 
            select(`Away Team Name`,
                   `Home Team Name`,
                    Attendance) %>%
                    pivot_longer(cols = c("Away Team Name","Home Team Name"),
                    names_to = "Tipo", values_to = "pais")

#### Top público ####

toppubli <- aggregate(x = publi$Attendance,
                 by = list(publi$pais),
                 FUN = sum,
                 na.rm = T) %>%  arrange(desc(x))


teste <- count(publi, pais) 
view(teste)
toppubli$pais <- toppubli$Group.1 

teste2 <- toppubli %>% 
              left_join(teste, by = "pais")

teste2$Group.1 <- NULL
topers <-  teste2 %>% arrange(desc(x)) %>% head(10)
xtable(topers)

topers2 <-  teste2 %>% arrange(desc(n)) %>% head(10)


#### Gráfico
topers$pt <- c("Brasil","Itália", "Argentina" ,"Alemanha FR","Inglaterra" ,"França",     
               "Espanha" ,"Holanda" ,"México" ,"Uruguai")
legendas <- str_squish(str_c(topers$mil))
topers$mil <- round(topers$x/1000000, digits = 2)
ggplot(topers) +
  aes(x = fct_reorder(pt, mil, .desc = F), y = mil, label = legendas) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .5),
    hjust = -0.3, #vjust = -0.5, 
    size = 3
  ) + 
    scale_y_continuous(limits=c(0, 6.5))+
  labs(x = "País", y = "Público (em milhões)") +
  coord_flip() +
  theme_estat()

ggsave("./gráficos/bar.png", width = 158, height = 93, units = "mm")

